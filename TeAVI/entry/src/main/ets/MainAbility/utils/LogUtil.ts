import hilog from '@ohos.hilog';

const TAG = "[ TeAVI ]";
const DOMAIN = 0x6688;

export default class LogUtil {

    static info(tag: string, log: string) {
        hilog.isLoggable(DOMAIN, TAG, hilog.LogLevel.INFO);
        hilog.info(DOMAIN, TAG, `--- ${TAG} ---> tag: ** ${tag} ** --> info: %{public}s ---`, log);
    }

    static debug(tag: string, log: string) {
        hilog.isLoggable(DOMAIN, TAG, hilog.LogLevel.DEBUG);
        hilog.debug(DOMAIN, TAG, `--- ${TAG} ---> tag: ** ${tag} ** --> info: %{public}s ---`, log);
    }

    static error(tag: string, log: string) {
        hilog.isLoggable(DOMAIN, TAG, hilog.LogLevel.ERROR);
        hilog.error(DOMAIN, TAG, `--- ${TAG} ---> tag: ** ${tag} ** --> info: %{public}s ---`, log);
    }

    static warn(tag: string, log: string) {
        hilog.isLoggable(DOMAIN, TAG, hilog.LogLevel.WARN);
        hilog.warn(DOMAIN, TAG, `--- ${TAG} ---> tag: ** ${tag} ** --> info: %{public}s ---`, log);
    }
}
