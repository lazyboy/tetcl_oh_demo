<center>tetclrdb</center>
---
<center>简化RDB关系型数据库操作</center>

# 工具集

- RdbUtil

# 安装使用
- 通过`npm`命令安装`tetclrdb`：
```
npm install @ohos/tetcrdb
```
- 在DevEco Studio内使用`tetclrdb`，工程级package.json内配置：
```
// package.json
{
    ...
    "dependencies": {
        "@ohos/tetclrdb": "1.0.0"
    }
}
```
# 约束
```
    DevEco Studio 3.1 Canary1
    HarmonyOS/OpenHarmony API 9
```
